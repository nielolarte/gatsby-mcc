/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it

import "./src/styles/active-directory.css"
import "./src/styles/clients.css"
import "./src/styles/company.css"
import "./src/styles/contact.css"
import "./src/styles/email-work-orders.css"
import "./src/styles/home.css"
import "./src/styles/licensing.css"
import "./src/styles/mobile.css"
import "./src/styles/mobile-sub-pages.css"
import "./src/styles/products-and-services-documents.css"
import "./src/styles/products-overview.css"
import "./src/styles/products.css"
import "./src/styles/support.css"
import "./src/styles/try-it-for-free.css"
