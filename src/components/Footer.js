import React from 'react'
import styled from 'styled-components'

import rem from '../utils/rem'

const Wrapper = styled.div`
  min-height: ${rem(10)};
  display: block;
  text-align: center;
  align-items: center;
  justify-content: center;
  padding: 1rem 0 1rem 0;
  background: #191919;
  color: white;
`

const Footer = styled.p`
  margin: 0.5rem 0;
`

export default () => (
  <Wrapper>
    <Footer>Quick Contact Toll Free: 1-800-720-6395 Fax: 1-800-720-6395</Footer>
    <Footer>Email: sales@maintenanceconnection.ca or Support: support@maintenanceconnection.ca</Footer>
    <Footer>'Maintenance Connection everywhere MCe/MCxLE aka XLE'</Footer>
  </Wrapper>
)
