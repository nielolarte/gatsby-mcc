import React from "react"
import { graphql } from "gatsby"

import Layout from '../components/Layout'
import SEO from "../components/Seo"

const IndexPage = ({data}) => (
  <Layout>
    <SEO title="Home" keywords={[`gatsby`, `application`, `react`]} />
    <div dangerouslySetInnerHTML={{ __html: data.wordpressPage.content }} />
  </Layout>
)
export default IndexPage
export const query = graphql`
  query {
    wordpressPage(slug: {eq: "home"}) {
      excerpt
      content
    }
  }
`
